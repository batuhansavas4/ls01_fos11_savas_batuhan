import java.util.Scanner;

/**
 * Klasse zum Anzeigen von Bus-Einsatzplaenen
 * 
 * @version 1.0 vom 10.12.2012
 * @author binz@oszimt.de
 */
public class Busplan {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int eingabe;

		Fahrer fahrer1 = new Fahrer(1422, "Kasuppke");
		Fahrer fahrer2 = new Fahrer(2732, "Konopke");
		Fahrer fahrer3 = new Fahrer(2031, "Marotzke");
		Fahrer fahrer4 = new Fahrer(1723, "Schulze");

		Bus bus1 = new Bus("X9", "B-VG 100");
		Bus bus2 = new Bus("X11", "B-VG 200");
		Bus tmpBus = null;

		bus1.setFahrerFrueh(fahrer1);
		bus1.setFahrerSpaet(fahrer2);

		bus2.setFahrerFrueh(fahrer3);
		bus2.setFahrerSpaet(fahrer4);

		do {
			System.out.println("\nSchichtuebersicht");
			System.out.println("(1) -> Schichtplan: " + bus1.getKfzZeichen()
					+ " (Linie: " + bus1.getLinie() + ")");
			System.out.println("(2) -> Schichtplan: " + bus2.getKfzZeichen()
					+ " (Linie: " + bus2.getLinie() + ")");
			System.out.println("(9) -> Programm beenden");

			System.out.print("\nIhre Wahl: ");
			eingabe = myScanner.nextInt();

			switch (eingabe) {
			case 1:
				tmpBus = bus1;
				break;
			case 2:
				tmpBus = bus2;
				break;
			}

			if (tmpBus != null) {
				System.out.println("\nSchichtplan: " + tmpBus.getKfzZeichen()
						+ " (Linie: " + tmpBus.getLinie() + ")");
				System.out.println("  Fruehschicht: "
						+ tmpBus.getFahrerFrueh().getPersonalnr() + " ("
						+ tmpBus.getFahrerFrueh().getName() + ")");
				System.out.println("  Spaetschicht: "
						+ tmpBus.getFahrerSpaet().getPersonalnr() + " ("
						+ tmpBus.getFahrerSpaet().getName() + ")");
			}
		} while (eingabe != 9);
	}
}
