
public class Fahrer {

	private int personalnr;
	private String name;

	// Konstruktor
	public Fahrer(int personalnr, String name) {
		this.personalnr = personalnr;
		this.name = name;
	}

	public int getPersonalnr() {
		return this.personalnr;
	}

	public String getName() {
		return this.name;
	}

}
