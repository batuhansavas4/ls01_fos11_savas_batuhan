
public class Feiertag extends Datum{
private String name;
public Feiertag() {


}
public Feiertag(int tag, int monat, int jahr, String name) {
	super(tag,monat,jahr);
	this.name= name;
}
public String getName() {
	return name;
}
public void setName(String Name) {
	this.name = name;
}
@Override
public String toString()
{
	return "Feiertag: "+ this.getName();
}
}
