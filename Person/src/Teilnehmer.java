
public class Teilnehmer extends Person {

	private String status;
	
	public Teilnehmer(){
	}
	public Teilnehmer(String nachname, String vorname, String status) {
	super(vorname,nachname);
	this.status = status;

	}
	public void  setStatus(String status) {
		this.status = status;
	}
	 public String getStatus() {
		 return status;
	}
	 public String toString()
		{
			return String.format("Person/in   | %20s | %20s | Status: %s%n", this.getNachname(), this.getVorname(), this.getStatus());		
		}
}

