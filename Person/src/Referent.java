
public class Referent extends Person {

	/** Attribute **/
	private String firma;
	private String spezialgebiet;
	
	/** Konstruktoren **/
	public Referent() {
	}

	public Referent(String vorname, String nachname, String firma, String spezialgebiet) {
		super(vorname, nachname); // super(...) ruft den Konstruktor der Oberklasse, hier Person, auf
		this.firma = firma;
		this.setSpezialgebiet(spezialgebiet);
	}


	/** Methoden **/

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	public String getSpezialgebiet() {
		return spezialgebiet;
	}

	public void setSpezialgebiet(String spezialgebiet) {
		this.spezialgebiet = spezialgebiet;
	}

	@Override
	public String toString()
	{
		return String.format("Referent/in   | %20s | %20s | Firma: %s%n", this.getNachname(), this.getVorname(), this.getFirma());		
	}


}
