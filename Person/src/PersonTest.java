
public class PersonTest {

	public static void main(String[] args) {
		/** Erstellen einer neuen Person **/
		Person p1 = new Person();
		p1.setVorname("Max");
		p1.setNachname("Mustermann");
		

		/** Erstellen eines Referenten **/
		Referent r1 = new Referent();
		r1.setVorname("Ramona");		// ...diese Methoden 
		r1.setNachname("Rarst");        // wurden von Person geerbt
		r1.setFirma("Klar EDV-AG");
		
		/** Erstellen eines Teilnehmers **/
		Teilnehmer t1 = new Teilnehmer();
		t1.setVorname("Thorsten");		// ...diese Methoden 
		t1.setNachname("Träger");       // wurden von Person geerbt
		t1.setStatus("Sch�ler");
		
		/** Ausgabe der Daten **/
		// Für die Ausgabe müssen Sie in der Klasse Person (und ggf. den Unterklassen) die Methode 
		// public String toString() implementieren.
		// Diese wird normalerweise von der Oberklasse java.lang.Object geerbebt - der "Mutter" aller Klassen
		// in Java - jede Klasse erbt von ihr!
		//System.out.println(p1);
		System.out.println(t1);
//		System.out.println(t1.getStatus());
		System.out.println(r1);

		
	}

}
